<?php

namespace NeoKree\Package\Layers\Service;

use NeoKree\Package\Layers\Data\BRepository;

class BServiceImpl implements BService
{
    private $repository;

    /**
     * BServiceImpl constructor.
     * @param BRepository $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }


    public function fetchSomething()
    {
        $this->repository->fetch();
    }
}