<?php

namespace NeoKree\Package\Layers\Data;

class FakeBRepository implements BRepository
{

    public function fetch()
    {
        return "something";
    }
}