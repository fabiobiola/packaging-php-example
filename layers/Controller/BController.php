<?php

namespace NeoKree\Package\Layers\Controller;

use NeoKree\Package\Layers\Service\BService;

class BController
{
    private $service;

    /**
     * BController constructor.
     * @param BService $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }


    function index() {
        $this->service->fetchSomething();
    }
}