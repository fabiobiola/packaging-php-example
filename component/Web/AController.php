<?php

namespace NeoKree\Package\Component\Web;

use NeoKree\Package\Component\A\AComponent;

class AController
{
    private $service;

    /**
     * AController constructor.
     * @param \NeoKree\Package\Component\A\AComponent $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }


    function index() {
        $this->service->saveSomething();
    }
}