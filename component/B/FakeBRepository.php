<?php

namespace NeoKree\Package\Component\B;

class FakeBRepository implements BRepository
{

    public function fetch()
    {
        return "something";
    }
}