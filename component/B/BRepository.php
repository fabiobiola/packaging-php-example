<?php

namespace NeoKree\Package\Component\B;

interface BRepository
{
    public function fetch();
}