<?php

namespace NeoKree\Package\Component\A;

interface ARepository
{
    public function save($message);
}