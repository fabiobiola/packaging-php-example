<?php

namespace NeoKree\Package\Feature\B;

class BController
{
    private $service;

    /**
     * BController constructor.
     * @param BService $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }


    function index() {
        $this->service->fetchSomething();
    }
}