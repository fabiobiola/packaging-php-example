<?php

namespace NeoKree\Package\Feature\B;

interface BRepository
{
    public function fetch();
}