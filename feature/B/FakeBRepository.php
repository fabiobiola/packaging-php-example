<?php

namespace NeoKree\Package\Feature\B;

class FakeBRepository implements BRepository
{

    public function fetch()
    {
        return "something";
    }
}