<?php

namespace NeoKree\Package\Feature\B;

class BServiceImpl implements BService
{
    private $repository;

    /**
     * BServiceImpl constructor.
     * @param \NeoKree\Package\Feature\B\BRepository $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }


    public function fetchSomething()
    {
        $this->repository->fetch();
    }
}