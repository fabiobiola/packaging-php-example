<?php

namespace NeoKree\Package\Feature\A;

class AController
{
    private $service;

    /**
     * AController constructor.
     * @param AService $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }


    function index() {
        $this->service->saveSomething();
    }
}