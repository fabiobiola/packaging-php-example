<?php

namespace NeoKree\Package\Explicit\Application\Ports;

interface BRepository
{
    public function fetch();
}