<?php

namespace NeoKree\Package\Explicit\Application\A;

use NeoKree\Package\Explicit\Application\Ports\ARepository;

class AServiceImpl implements AService
{
    private $repository;

    /**
     * AServiceImpl constructor.
     * @param \NeoKree\Package\Explicit\Application\Ports\ARepository $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }


    public function saveSomething()
    {
        $this->repository->save("something");
    }
}