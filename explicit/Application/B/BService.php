<?php

namespace NeoKree\Package\Explicit\Application\B;

interface BService
{
    public function fetchSomething();
}