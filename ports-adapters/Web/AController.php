<?php

namespace NeoKree\Package\PortsAndAdapters\Web;

use NeoKree\Package\PortsAndAdapters\Domain\AService;

class AController
{
    private $service;

    /**
     * AController constructor.
     * @param \NeoKree\Package\PortsAndAdapters\Domain\AService $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }


    function index() {
        $this->service->saveSomething();
    }
}