<?php

namespace NeoKree\Package\PortsAndAdapters\Domain;

class BServiceImpl implements BService
{
    private $repository;

    /**
     * BServiceImpl constructor.
     * @param BRepository $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }


    public function fetchSomething()
    {
        $this->repository->fetch();
    }
}