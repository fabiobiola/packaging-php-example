<?php

namespace NeoKree\Package\PortsAndAdapters\Domain;

interface BRepository
{
    public function fetch();
}