<?php

namespace NeoKree\Package\PortsAndAdapters\Domain;

interface AService
{
    public function saveSomething();
}