<?php

namespace NeoKree\Package\PortsAndAdapters\Database;

use NeoKree\Package\PortsAndAdapters\Domain\BRepository;

class FakeBRepository implements BRepository
{

    public function fetch()
    {
        return "something";
    }
}